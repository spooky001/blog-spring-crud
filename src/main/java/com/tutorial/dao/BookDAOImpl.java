package com.tutorial.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tutorial.model.Book;

@Repository
public class BookDAOImpl implements BookDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void addBook(Book book) {
		getCurrentSession().save(book);
	}

	public void updateBook(Book book) {
		Book bookToUpdate = getBook(book.getId());
		/*
		bookToUpdate.setName(book.getName());
		bookToUpdate.setRating(book.getRating());
		getCurrentSession().update(bookToUpdate);
		*/
		
		
		bookToUpdate.setTitle(book.getTitle());
		bookToUpdate.setPublisher(book.getPublisher());
		bookToUpdate.setPrice(book.getPrice());
		bookToUpdate.setBookPages(book.getBookPages());
		getCurrentSession().update(bookToUpdate);
		
		
		
	}

	public Book getBook(int id) {
		Book book = (Book) getCurrentSession().get(Book.class, id);
		return book;
	}

	public void deleteBook(int id) {
		Book book = getBook(id);
		if (book != null)
			getCurrentSession().delete(book);
	}

	@SuppressWarnings("unchecked")
	public List<Book> getBooks() {
		return getCurrentSession().createQuery("from Book").list();
	}

}
