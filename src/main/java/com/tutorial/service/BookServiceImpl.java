package com.tutorial.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tutorial.dao.BookDAO;
import com.tutorial.model.Book;

@Service
@Transactional
public class BookServiceImpl implements BookService {
	
	@Autowired
	private BookDAO bookDAO;

	public void addBook(Book book) {
		bookDAO.addBook(book);		
	}

	public void updateBook(Book book) {
		bookDAO.updateBook(book);
	}

	public Book getBook(int id) {
		return bookDAO.getBook(id);
	}

	public void deleteBook(int id) {
		bookDAO.deleteBook(id);
	}
	
	
	public List<Book> getBooks() {
		return bookDAO.getBooks();
	}

}
