package com.tutorial.service;

import java.util.List;

import com.tutorial.model.Book;

public interface BookService {
	
	public void addBook(Book book);
	public void updateBook(Book book);
	public Book getBook(int id);
	public void deleteBook(int id);
	public List<Book> getBooks();

}
