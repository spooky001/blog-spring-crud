-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Wersja serwera:               5.5.24-log - MySQL Community Server (GPL)
-- Serwer OS:                    Win32
-- HeidiSQL Wersja:              8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Zrzut struktury bazy danych library
CREATE DATABASE IF NOT EXISTS `library` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci */;
USE `library`;


-- Zrzut struktury tabela library.book
CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) COLLATE utf8_polish_ci NOT NULL,
  `bookPages` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `publisher` varchar(20) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- Dumping data for table library.book: ~7 rows (około)
DELETE FROM `book`;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` (`id`, `title`, `bookPages`, `price`, `publisher`) VALUES
	(1, 'Title', NULL, 123, 'Window'),
	(2, 'asdasd4', NULL, 123, 'qwewqe'),
	(3, 'asdasd4', NULL, 123, 'qwewqe'),
	(5, 'Strony internetowe, ', 250, 123, 'Window'),
	(6, 'Strony internetowe,', NULL, 123, 'Window'),
	(7, 'qwerty', 2501, 123, 'Window'),
	(9, 'Title', 2502, 123, 'Window');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
